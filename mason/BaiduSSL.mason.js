<mason>
Charset=utf8
Author=ShuangYa
Created=2015/04/07
Updated=2015/04/08
name=BaiduSearch SSL Redirector
version=1.1
Website=
Comment=
Description=BaiduSearch SSL Redirector
Usage=
Url=^http://www\.baidu\.com(|/|/index.*|/baidu.*|/s.*)$
</mason>
<parts>
part1=.@@@L3
</parts>
<part1>
function _masonRedirect(spec){
    spec=spec.replace(/^http/,'https').replace(/\&tn=(.*?)&/,'&').replace(/\&tn=(.*?)$/,'');
	return spec;
}
</part1>
<mason>
Charset=utf8
Author=ShuangYa
Created=2014/06/13
Updated=2014/09/06
name=Google Lib Redirector
version=1.2
Website=
Comment=
Description=Google Lib Redirector
Usage=
Url=^http([s]?)://(fonts|ajax)\.googleapis\.com/(.*?)$
</mason>
<parts>
part1=.@@@L3
</parts>
<part1>
function _masonRedirect(spec){
	if (spec.match(/^https/)===null) { //非https，使用360的镜像
		return spec.replace('googleapis.com','useso.com');
	} else { //https，使用科大的镜像
		return spec.replace('googleapis.com','lug.ustc.edu.cn');
	}
}
</part1>
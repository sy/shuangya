# 注意事项

***

* ACFun的签到信息并不是零点更新，所以Cron请不要设置的太早

# 使用方法

***

## SAE

* 首先到SAE开启MySQL服务和TaskQueue服务

* 修改以下内容

* acfun.php第八行、第九行，修改为对应的东西

* acfun.sql，修改第一行的acfun_为acfun.php第八行的内容

* config.yaml第一、二行，修改为已存在的config.yaml中相同。第七行的1修改为4-10任意数字，然后替换掉原有的config.yaml

* 之后上传到SAE

* 然后进入phpMyAdmin，导入acfun.sql

## 非SAE使用方法

* 修改以下内容：

* 删除原有的acfun.php，将acfun_default.php重命名为acfun.php

* acfun.php第八行到第十三行，修改为对应的东西

* acfun.sql，修改第一行的acfun_为acfun.php第八行的内容

* 然后设置好Cron，指向acfun.php

* 然后进入phpMyAdmin，导入acfun.sql

## 添加帐号的方法

* 进入phpmyadmin，点acfun_cookie表

* 点上面的“插入”

* id留空

* last_sign写2014-08-28

* auth和sha1分别填写acfun的cookie里的auth_key和auth_key_ac_sha1
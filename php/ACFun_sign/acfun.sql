CREATE TABLE `acfun_cookie` (
	`id` BIGINT( 10 ) NOT NULL AUTO_INCREMENT ,
	`last_sign` DATE NOT NULL ,
	`on_line` int(1) NOT NULL ,
	`auth` VARCHAR( 255 ) NOT NULL ,
	`sha1` VARCHAR( 255 ) NOT NULL ,
	PRIMARY KEY ( `id` ),
	INDEX (`last_sign`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
<?php
@error_reporting(E_ALL &~ E_NOTICE);
header('Content-Type: text/html; charset=utf-8');
$out='';
/*****************************************/
/*           用户自行设置的变量          */
/*****************************************/
$dbpre='acfun_'; //数据表前缀
define('SAE_MYSQL_HOST_M',''); //数据库服务器
define('SAE_MYSQL_PORT',''); //数据库端口
define('SAE_MYSQL_DB',''); //数据库名称
define('SAE_MYSQL_USER',''); //数据库用户名
define('SAE_MYSQL_PASS',''); //数据库密码
/*****************************************/
/*               连接数据库              */
/*****************************************/
$dsn='mysql:host='.SAE_MYSQL_HOST_M.';port='.SAE_MYSQL_PORT.';dbname='.SAE_MYSQL_DB.';charset=utf8';
$db=new PDO($dsn,SAE_MYSQL_USER,SAE_MYSQL_PASS); //初始化PDO
$self=$_SERVER['PHP_SELF'];
/*****************************************/
/*                常用函数               */
/*****************************************/
function curl_get($url,$cookie){
	$ch=curl_init($url);
	$header=array('User-Agent: Mozilla/5.0 (Windows NT 5.1; rv:35.0) Gecko/20100101 Firefox/35.0','Referer: http://www.acfun.tv/member/');
	curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch,CURLOPT_HEADER,0);
	curl_setopt($ch,CURLOPT_COOKIE,$cookie);
	$get_url=curl_exec($ch);
	@curl_close($ch);
	return $get_url;
}
function mkcookie ($auth_key,$auth_key_ac_sha1) {
	return 'auth_key='.$auth_key.'; auth_key_ac_sha1='.$auth_key_ac_sha1;
}
/*****************************************/
/*                进行签到               */
/*****************************************/
$date=date('Y-m-d');
$st=$db->prepare('SELECT * FROM `'.$dbpre.'cookie` WHERE last_sign <> ? LIMIT 0,10');
$st->bindValue(1,$date);
$st->execute(); //查询
$st->setFetchMode(PDO::FETCH_ASSOC);
while ($row=$st->fetch()) {
	$qiandao=curl_get('http://www.acfun.tv/member/checkin.aspx',mkcookie($row['auth'],$row['sha1']));
	$qiandao=json_decode($qiandao,1);
	if (!is_array($qiandao)) {
		echo '<font color="#999999">'.$row['id'].'</font><br>';
	} elseif (isset($qiandao['status']) && $qiandao['status']==401) { //删除失效cookie
		$tmp=$db->prepare('DELETE FROM `'.$dbpre.'cookie` WHERE id = ?');
		$tmp->bindValue(1,$row['id']);
		$tmp->execute();
		echo '<font color="red">'.$row['id'].'</font><br>';
	} elseif ($qiandao['success']==TRUE || $qiandao['result']=='您今天已签到过') { //成功
		$tmp=$db->prepare('UPDATE `'.$dbpre.'cookie` SET last_sign = \''.$date.'\' WHERE id = ?');
		$tmp->bindValue(1,$row['id']);
		$tmp->execute();
		echo '<font color="green">'.$row['id'].'</font><br>';
	} else { //未知
		echo '<font color="black">'.$row['id'].'</font><br>';
	}
}
?>
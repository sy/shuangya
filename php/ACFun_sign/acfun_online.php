<?php
@error_reporting(E_ALL &~ E_NOTICE);
header('Content-Type: text/html; charset=utf-8');
/*****************************************/
/*           用户自行设置的变量          */
/*****************************************/
$dbpre='acfun_'; //数据表前缀
/*****************************************/
/*               连接数据库              */
/*****************************************/
$dsn='mysql:host='.SAE_MYSQL_HOST_M.':'.SAE_MYSQL_PORT.';dbname='.SAE_MYSQL_DB.';charset=utf8';
$db=new PDO($dsn,SAE_MYSQL_USER,SAE_MYSQL_PASS); //初始化PDO
$self=$_SERVER['PHP_SELF'];
/*****************************************/
/*                常用函数               */
/*****************************************/
function curl_get($url,$cookie){
	$ch=curl_init($url);
	$header=array('User-Agent: Mozilla/5.0 (Windows NT 5.1; rv:35.0) Gecko/20100101 Firefox/35.0','Referer: http://www.acfun.tv/member/');
	curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch,CURLOPT_HEADER,0);
	curl_setopt($ch,CURLOPT_COOKIE,$cookie);
	$get_url=curl_exec($ch);
	@curl_close($ch);
	return $get_url;
}
function mkcookie ($auth_key,$auth_key_ac_sha1) {
	return 'auth_key='.$auth_key.'; auth_key_ac_sha1='.$auth_key_ac_sha1;
}
/*****************************************/
/*                进行签到               */
/*****************************************/
$st=$db->prepare('SELECT * FROM `'.$dbpre.'cookie` WHERE on_line = ? LIMIT 0,10');
$st->bindValue(1,'0');
$st->execute(); //查询
$st->setFetchMode(PDO::FETCH_ASSOC);
while ($row=$st->fetch()) {
	$online=curl_get('http://www.acfun.tv/online.aspx?uid='.$row['auth'],mkcookie($row['auth'],$row['sha1']));
	$online=json_decode($online);
	if (!is_array($online)) {
		echo '<font color="#999999">'.$row['id'].'</font><br>';
	} elseif ($online['isdisabled']==TRUE) { //在线完
		$tmp=$db->prepare('UPDATE `'.$dbpre.'cookie` SET on_line = 1 WHERE id = ?');
		$tmp->bindValue(1,$row['id']);
		$tmp->execute();
		echo '<font color="black">'.$row['id'].'</font><br>';
	} elseif ($online['success']==TRUE) { //成功
		echo '<font color="green">'.$row['id'].'</font><br>';
	} else { //未知
		echo '<font color="red">'.$row['id'].'</font><br>';
	}
}
$tmp=$db->prepare('SELECT count(*) as num FROM `'.$dbpre.'cookie` WHERE on_line = ?');
$tmp->bindValue(1,'0');
$tmp->execute(); //查询
$tmp->setFetchMode(PDO::FETCH_ASSOC);
$tmp=$tmp->fetch();
if ($tmp['num']!=0) { //还有没有签到的
	$queue=new SaeTaskQueue($task);
	$queue->addTask($self);
}
?>
# 注意事项

***

* 如果使用php-win.exe调用，可以没有界面

* ACFun的签到信息并不是零点更新，所以定时任务请不要设置的太早

* 在线程序可以一直开着，会默默的后台运行着，需要结束请用“任务管理器”之类的软件结束进程“php-win.exe”（如果使用php-win.exe调用的话）

# 使用方法

***

* 修改以下内容：

* acfun.php和acfun_online.php第7行，修改为对应的东西

* acfun.php和acfun_online.php第11行，修改为：`$dsn='mysql:host={MySQL服务器地址};port={MySQL服务器端口};dbname={MySQL数据库名};charset=utf8';`，例如：`$dsn='mysql:host=localhost;port=3306;dbname=test;charset=utf8';`

* acfun.php和acfun_online.php第12行，修改为：`$db=new PDO($dsn,'{MySQL用户名}','{MySQL密码}');`，例如：`$db=new PDO($dsn,'root','root');`

* acfun.sql，修改第一行的acfun_为acfun.php/acfun_online.php第7行的内容

* 建议设置acfun_online.php开机/登录时启动，acfun.php闲时启动

* 以Windows为例：新建任务计划，“运行”填`E:\Soft\php\php-win.exe E:\Soft\php\acfun_online.php`（本例中php安装在E:\Soft\php\下），“计划任务”选择“在登录时”
<?php
@error_reporting(E_ALL &~ E_NOTICE);
header('Content-Type: text/html; charset=utf-8');
/*****************************************/
/*           用户自行设置的变量          */
/*****************************************/
$dbpre='acfun_'; //数据表前缀
/*****************************************/
/*               连接数据库              */
/*****************************************/
$dsn='mysql:host=localhost;port=3306;dbname=test;charset=utf8';
$db=new PDO($dsn,'root','root'); //初始化PDO
$self=$_SERVER['PHP_SELF'];
/*****************************************/
/*                常用函数               */
/*****************************************/
function curl_get($url,$cookie){
	$ch=curl_init($url);
	$header=array('User-Agent: Mozilla/5.0 (Windows NT 5.1; rv:35.0) Gecko/20100101 Firefox/35.0','Referer: http://www.acfun.tv/member/');
	curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch,CURLOPT_HEADER,0);
	curl_setopt($ch,CURLOPT_COOKIE,$cookie);
	$get_url=curl_exec($ch);
	@curl_close($ch);
	return $get_url;
}
function mkcookie ($auth_key,$auth_key_ac_sha1) {
	return 'auth_key='.$auth_key.'; auth_key_ac_sha1='.$auth_key_ac_sha1;
}
/*****************************************/
/*                进行签到               */
/*****************************************/
$date=date('Y-m-d');
$st=$db->prepare('SELECT * FROM `'.$dbpre.'cookie` WHERE last_sign <> ?');
$st->bindValue(1,$date);
$st->execute(); //查询
$st->setFetchMode(PDO::FETCH_ASSOC);
while ($row=$st->fetch()) {
	$qiandao=curl_get('http://www.acfun.tv/member/checkin.aspx',mkcookie($row['auth'],$row['sha1']));
	$qiandao=json_decode($qiandao,1);
	if (!is_array($qiandao)) {
		echo $row['id'],':curl fail',"\r\n";
	} elseif ($qiandao['success']==TRUE || $qiandao['result']=='您今天已签到过') { //成功
		$tmp=$db->prepare('UPDATE `'.$dbpre.'cookie` SET last_sign = \''.$date.'\' WHERE id = ?');
		$tmp->bindValue(1,$row['id']);
		$tmp->execute();
		echo $row['id'],':success',"\r\n";
	} else { //未知
		echo $row['id'],':unknow',"\r\n";
	}
}
?>
<?php
/**
 * 主文件
 * 
 * @author ShuangYa
 * @package TiebaMonitor
 * @link http://www.sylingd.com/
 * @copyright Copyright (c) 2016 ShuangYa
 * @license http://lab.sylingd.com/go/TiebaMonitor/license
 */

//以下为可配置选项
define('INTERVAL', 1); //每次抓取间隔时间，单位：秒
$config = [
	'level' => 10,
	'BDUSS' => '', //吧务账号BDUSS
	'kw' => '', //贴吧名称
	'word' => '广告测试|敏感词测试', //关键词，将忽略大小写
	'ban' => 3, //封禁天数，0为不封禁
	'reason' => '涉嫌广告', //封禁理由
	'whiteList' => '泷涯,文科980195412' //白名单ID列表，以“,”隔开，列表内的ID均不会被删帖
]
//配置结束

//开始
require('SDK/autoload.php');
require('TiebaMonitor.php');
$monitor = new TiebaMonitor($config);
//开始监控
while (TRUE) {
	sleep(INTERVAL);
	$monitor->check();
}
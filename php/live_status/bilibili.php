<?php
require('function.php');
function getStatus($id) {
	$rs = fetchUrl('http://live.bilibili.com/h5/' . $id, [
		'header' => [
			'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 9_2_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13D15 Safari/601.1'
		]
	]);
	preg_match('/live_status = (\d+)/', $rs, $matches);
	return $matches[1];
}
//含义：1为正在直播，0为未开播
$status = getStatus(133467);
echo $status;
echo "\n";
$status = getStatus(5050);
echo $status;
echo "\n";
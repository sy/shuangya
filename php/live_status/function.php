<?php
function fetchUrl($url, $data = []) {
	$ch = curl_init($url);
	if (isset($data['header'])) {
		curl_setopt($ch, CURLOPT_HTTPHEADER, $data['header']);
	}
	if (isset($data['cookie'])) {
		curl_setopt($ch, CURLOPT_COOKIE, $data['cookie']);
	}
	if (isset($data['post'])) {
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data['post']);
	}
	if (substr($url, 0, 8) === 'https://') {
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	}
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	$r = curl_exec($ch);
	@curl_close($ch);
	return $r;
}
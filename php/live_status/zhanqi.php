<?php
require('function.php');
function getRoomId($name) {
	$page = fetchUrl('https://www.zhanqi.tv/' . $name);
	preg_match('/"RoomId":(\d+),/', $page, $matches);
	return $matches[1];
}
function getStatus($id) {
	$rs = fetchUrl('https://www.zhanqi.tv/api/public/room.liveparam?room_id=' . $id);
	$rs = json_decode($rs, 1);
	return $rs['data']['status'];
}
//含义：4为正在直播，0为未开播
$id = getRoomId('yeyuan');
$status = getStatus($id);
echo $status;
echo "\n";
$id = getRoomId('edmunddzhang');
$status = getStatus($id);
echo $status;
echo "\n";
$id = getRoomId('1152459');
$status = getStatus($id);
echo $status;
echo "\n";
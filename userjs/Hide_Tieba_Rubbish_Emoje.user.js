// ==UserScript==
// @name        垃圾表情隐藏
// @namespace   blog.sylingd.com
// @include     http://tieba.baidu.com/p/*
// @include     https://tieba.baidu.com/p/*
// @version     2
// ==/UserScript==

(function(){
	var list = unsafeWindow.document.querySelectorAll('.l_post');
	for (var i in list) {
		try {
			if (JSON.parse(list[i].dataset.field).content.post_index == 1 && list[i].querySelector('.p_content').innerHTML.indexOf('#春节表情包#') > 0) {
				list[i].style.display = "none";
			}
		} catch (err) {
			//do nothing
		}
	}
})();

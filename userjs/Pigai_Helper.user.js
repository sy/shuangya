// ==UserScript==
// @name        Pigai Helper
// @namespace   blog.sylingd.com
// @include     http://*.pigai.org/index.php*
// @version     1
// @grant       GM_setClipboard
// @updateURL   https://git.oschina.net/sy/shuangya/raw/master/userjs/Pigai_Helper.user.js
// @downloadURL https://git.oschina.net/sy/shuangya/raw/master/userjs/Pigai_Helper.user.js
// ==/UserScript==

(function(){
	//添加复制按钮
	if (document.getElementById('request_y') !== null) {
		var text = document.getElementById('request_y').innerHTML;
		text = text.replace(/<[^>]+>/g,""); //去掉所有的html标记
		text = text.replace(/&nbsp;/g, ' '); //替换空格
		text = text.trim();
		//添加复制按钮
		var copyBtn = document.createElement('a');
		copyBtn.innerHTML = '复制';
		copyBtn.style.marginLeft = "5px";
		copyBtn.style.cursor = "pointer";
		copyBtn.style.color = "rgb(41, 122, 204)";
		document.getElementById('request_y').previousElementSibling.appendChild(copyBtn);
		copyBtn.addEventListener('click', function() {
			GM_setClipboard(text);
		}, false);
	}
	//添加粘贴功能
	if (document.getElementById('copy_re') !== null) {
		var copy_btn = document.getElementById('copy_re');
		var paste_btn = document.createElement('a');
		paste_btn.innerHTML = '粘贴内容';
		paste_btn.style.cursor = 'pointer';
		paste_btn.style.marginRight = '5px';
		copy_btn.parentElement.insertBefore(paste_btn, copy_btn);
		paste_btn.addEventListener('click', function() {
			var text = document.getElementById('contents');
			var val = window.prompt('请粘贴内容');
			if (val) {
				var startPos = text.selectionStart;
				var endPos = text.selectionEnd;
				text.innerHTML = text.innerHTML.substring(0, startPos) + val + text.innerHTML.substring(endPos);
				text.focus();
				text.selectionStart = startPos + val.length;
				text.selectionEnd = startPos + val.length;
			}
		}, false);
	}
})();
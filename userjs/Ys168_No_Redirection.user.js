// ==UserScript==
// @name        Ys168 No Redirection
// @namespace   blog.sylingd.com
// @description Ys168 No Redirection
// @include     http://*.ys168.com/*
// @include     https://*.ys168.com/*
// @version     2
// @updateURL   https://git.oschina.net/sy/shuangya/raw/master/userjs/Ys168_No_Redirection.user.js
// @downloadURL https://git.oschina.net/sy/shuangya/raw/master/userjs/Ys168_No_Redirection.user.js
// ==/UserScript==

(function () {
	var _mldq = window.mldq;
	if (!_mldq) return ;
	var sKey = '/note/fd.htm?';
	window.mldq = function () {
		console.log(arguments);
		var ret = _mldq.apply(this, arguments);
		var eLinks = document.getElementById('menuList').querySelectorAll('a');
		for (var i=0; i<eLinks.length; i++) {
			if (eLinks[i].getAttribute('href').indexOf(sKey) > 0) {
				eLinks[i].href = eLinks[i].href.substr(eLinks[i].href.indexOf(sKey) + sKey.length);
				eLinks[i].removeAttribute('target');
			}
		}
		return ret;
	};
})();